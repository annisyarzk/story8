$(document).ready(function() {
	search("icarus");
});

function baca(){
	var key = $('#box').val();
	$('#box').val("");
	search(key);
}

function search(key){
	$('#result').empty();
	$.ajax({
		method: 'GET',
		url: 'getData/' + key,
		success: function(response){
			console.log(response);
			for (let i=0; i < response.items.length; i++){

				$('#result').append('<tr>');
				var cover =  response.items[i].volumeInfo.imageLinks;

				if(typeof cover != 'undefined'){
					cover = cover.thumbnail;
					$('#result').append('<td><img src="' + cover + '"></td>');
				}

				else{
					$('#result').append('<td>No image available</td>');
				}

				var title = response.items[i].volumeInfo.title;
				$('#result').append('<td>' + title + '</td>');

				var author = response.items[i].volumeInfo.authors;
				$('#result').append('<td>' + author + '</td>');
				$('#result').append('</tr>');	
			}
		}
	});
}