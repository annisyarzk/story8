from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class UnitTest(TestCase):
	def test_apakah_url_bisa_diakses(self):
		response = Client().get('//')
		self.assertEqual(response.status_code, 200)

	def test_views_memanggil_index_html(self):
		response = Client().get('//')
		self.assertTemplateUsed(response, 'index.html')

	def test_halaman_utama_pakai_fungsi_index(self):
		function = resolve('/')
		self.assertEqual(function.func, index)

	def test_pakai_fungsi_get_data(self):
		function = resolve('/getData/<str:key>')
		self.assertEqual(function.func, get_data)

	def test_include_jquery(self):
		response = Client().get('//')
		content = response.content.decode('utf8')
		self.assertIn("https://code.jquery.com/jquery/3.4.1.js", content)

	def test_ada_script(self):
		response = Client().get('//')
		content = response.content.decode('utf8')
		self.assertIn("<script", content)

	def test_ada_search_box(self):
		response = Client().get('//')		
		content = response.content.decode('utf8')
		self.assertIn("<input", content)
		self.assertIn("text", content)

	def test_ada_button_cari(self):
		response = Client().get('//')
		content = response.content.decode('utf8')
		self.assertIn("<input", content)
		self.assertIn("submit", content)

	def test_ada_table_hasil(self):
		response = Client().get('//')
		content = response.content.decode('utf8')
		self.assertIn("<table", content)


# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import time

# class FunctionalTest(TestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		super(FunctionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(FunctionalTest, self).tearDown()

# 	def test_cari_buku(self):
# 		selenium = self.selenium
# 		# buka link yg mau dites
# 		selenium.get('http://127.0.0.1:8000/')

# 		# cari elemen
# 		searchbox = selenium.find_element_by_id('box')
# 		button = selenium.find_element_by_id('button')

# 		# isi search box
# 		searchbox.send_keys('icarus')

# 		# klik tombol cari buku
# 		button.send_keys(Keys.RETURN)
